<div id="welcome">
	<div class="row">
		<div class="wlcLeft col-5 fl">
			<h4>WELCOME!</h4>
			<h1>ALL ABOUT COMFORT UTAH <span>HEATING & AIR</span> </h1>
			<p>All about comfort Utah Heating & Air is proud to continue serving Utah & Salt Lake counites <br> with honest,professional heating and air condition services. After 15 years of work with some of the largest companies in Utah i decided to begin my own business focused on providing homeowners with exceptional service.</p>
			<p>We’re dedicated to providing honest services to each and every homeowner we work with. Well provide you with all the necessary information you need to make an informed decision on important heating and cooling needs. We consistently keep  up with new technologies and we’re able to adapt new technology into older/Existing homes. </p>
			<p>So give our team a call at (801)-541-2931to learn about all our heating and cooling services. </p>
			<a href="contact#content" class="btn2">CONTACT US</a>
		</div>
		<div class="clearfix"></div>
		<img src="public/images/content/welcomeIMG.jpg" alt="Welcome image" class="wlcIMG">
	</div>
</div>
<div id="services1">
	<div class="row">
		<dl>
			<dt> <img src="public/images/content/svc1IMG1.jpg" alt="Service Image 1"> </dt>
			<dd>
				<h2>AIR CONDITIONING SERVICE</h2>
				<p>When you call All About Comfort Utah for professional maintenance of your cooling system,we deliver greater return for your Investment. To achieve Factory Authorization, we adheres to strict business practices, training standards, and service procedures. That translate into a team of fully qualified and experienced technicians,who take pride in exceptional job performance.We earn the trust of homeowners throughout the entire the Utah & Salt lake valley.</p>
			</dd>
		</dl>
		<dl>
			<dt> <img src="public/images/content/svc1IMG2.jpg" alt="Service Image 2"> </dt>
			<dd>
				<h2>QUALITY HOME HEATING SERVICES</h2>
				<p>There’s no question that every brand and age of furnace requires professional, annual maintenance to meet demand and expectations. Unfortunately, not all contractors uphold the same standards of service. When you rely on All About Comfort Utah, you get far more than your money’s worth. You take advantage of our commitment to quality. From our business and customer service practices,to our expertise and attention to detail. All About Comfort is the choice you can trust for your heating service.</p>
			</dd>
		</dl>
		<dl>
			<dt> <img src="public/images/content/svc1IMG3.jpg" alt="Service Image 3"> </dt>
			<dd>
				<h2>RELIABLE A/C SERVICES</h2>
				<p>Providing proactive air conditioning service since 1994, we know all the reasons homeowners procrastinate and we’ve optimized our services to eliminate them. With flexible scheduling, prompt arrival, swift turnaround and neat and tidy job sites, All About Comfort Utah ensures a convenient and pleasurable experience. Our licensed A/C technicians are equipped with advanced tools, leading-edge diagnostic equipment and top quality replacement parts to resolve any challenge and restore equipment to peak condition. Also, our experts can install new air conditioning units to help efficiently control the temperature in your home.</p>
			</dd>
		</dl>
	</div>
</div>
<div id="area">
	<div class="row">
		<div class="areaLeft col-8 fl">
			<h2>SERVING UTAH & SLC COUNTIES
				<span>YOUR AIR QUALITY SOLUTION SPECIALISTS</span>
			</h2>
			<p>All About Comfort Utah Heating and Air can really improve your home or business  air quality without major downtime or cost. We’ll give you a consult and a free quote for exceptional services.</p>
			<a href="contact#content" class="btn">CONTACT US</a>
		</div>
		<img src="public/images/content/aircon.png" alt="aircon" class="aircon">
		<div class="clearfix"></div>
	</div>
</div>
<div id="services2">
	<div class="row">
		<div class="svc2Right col-5 fr">
			<h3>WHAT WE DO</h3>
			<h2>OUR SERVICES</h2>
			<ul>
				<li>Central A/C</li>
				<li>Central Humidifiers</li>
				<li>Furnace</li>
				<li>Heat Pump</li>
				<li>Swamp Cooler</li>
				<li>Venting</li>
				<li>Water Heater</li>
				<li>And MORE!</li>
			</ul>
			<a href="contact#content" class="btn2">CONTACT US</a>
			<img src="public/images/common/military.png" alt="military discount" class="military">
			<img src="public/images/common/log.png" alt="log" class="log">
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div id="why">
	<div class="row">
		<h2>WHY CHOOSE US</h2>
		<div class="container">
			<dl>
				<dt> <img src="public/images/content/why1.jpg" alt="Why choose us 1"> </dt>
				<dd>24/7 HVAC <br>SERVICE</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/why2.jpg" alt="Why choose us 1"> </dt>
				<dd>WE CARE ABOUT <br>THE DETAILS</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/why3.jpg" alt="Why choose us 1"> </dt>
				<dd>WE ARE EXPERIENCED <br>AND PROFESSIONALS</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/why4.jpg" alt="Why choose us 1"> </dt>
				<dd>HONEST AND <br>RELIABLE SERVICE</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/why5.jpg" alt="Why choose us 1"> </dt>
				<dd>CUSTOMER SATISFACTION <br>GUARANTEED</dd>
			</dl>
		</div>
	</div>
</div>
<div id="works">
	<div class="row">
		<div class="worksLeft col-7 fl">
			<h2>RECENT WORKS</h2>
			<div class="container">
				<img src="public/images/content/recent1.jpg" alt="Recent Work 1">
				<img src="public/images/content/recent2.jpg" alt="Recent Work 2">
				<img src="public/images/content/recent3.jpg" alt="Recent Work 3">
				<img src="public/images/content/recent4.jpg" alt="Recent Work 4">
			</div>
			<img src="public/images/common/snowflake.png" alt="snowflake" class="sf">
		</div>
		<div class="worksRight col-5 fl">
			<div class="container">
				<h2>CONTACT US <span> LET US KNOW WHAT YOU NEED</span></h2>
				<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
					<label><span class="ctc-hide">Name</span>
						<input type="text" name="name">
					</label>
					<label><span class="ctc-hide">Email</span>
						<input type="text" name="email">
					</label>
					<label><span class="ctc-hide">Phone</span>
						<input type="text" name="phone">
					</label>
					<label><span class="ctc-hide">Message</span>
						<textarea name="message" cols="30" rows="10"></textarea>
					</label>
					<label for="g-recaptcha-response"><span class="ctc-hide recap">Recaptcha</span></label>
					<div class="g-recaptcha"></div>
					<label>
						<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
					</label><br>
					<?php if( $this->siteInfo['policy_link'] ): ?>
					<label>
						<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
					</label>
					<?php endif ?>
					<button type="submit" class="ctcBtn" disabled>SUBMIT FORM</button>
				</form>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div id="contact">
	<div class="row">
		<h2>CONTACT US</h2>
		<p>ALL ABOUT COMFORT UTAH Heating and Air, is committed to providing our clients with independent professional assistance on their projects. We are capable of providing Heating and Air conditioning services all over Utah. Give us a call today!</p>
		<div class="container">
			<dl>
				<dt>
					<img src="public/images/common/sprite.png" alt="email" class="email">
					<span>Email</span>
				</dt>
				<dd>
					<?php $this->info(["email","mailto","cntEmail"]);?>
				</dd>
			</dl>
			<dl>
				<dt>
					<img src="public/images/common/sprite.png" alt="phone" class="phone">
					<span>Phone</span>
				</dt>
				<dd>
					<?php $this->info(["phone","tel","cntPhone"]);?>
				</dd>
			</dl>
			<dl>
				<dt>
					<img src="public/images/common/sprite.png" alt="location" class="location">
					<span>Location</span>
				</dt>
				<dd>
					<?php $this->info("address");?>
				</dd>
			</dl>
		</div>
	</div>
</div>
